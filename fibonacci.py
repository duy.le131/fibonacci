def fibonacci (i):
    if i == 0:
        return 0
    elif i == 1:
        return 1 
    else:
        return fibonacci(i-2) + fibonacci(i-1)

i = int(input("Enter the number:"))

print("Fibonacci series:")

for i in range(0,i):

    print(fibonacci(i))